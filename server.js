console.log('Starting VLC launcher...')

const express = require('express')
const app = express()
const port = 4444
const shelljs = require('shelljs')
const path = require('path')

app.use(express.json()) // for reading POST requests (Content-type: application/json)

app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './', 'client.html'))
})

app.get('/playing', (req, res) => {
	let playing = shelljs.exec('pidof vlc', {silent: true})
	if (playing.stdout === '') { // probably a better way to do this..
		playing = []
	}
	else {
		playing = playing.replace(/\n/, '').split(' ')
	}
	res.send(JSON.stringify(playing))
})

app.post('/', (req, res) => {
	let url = req.body.url
	console.log(`Trying to fire up ${url} in VLC`)
	res.send('Got your request, firing up...')
	shelljs.exec('./launch-vlc.sh ' + url, {silent: true}, function(res) {
		// this doesn't fire until VLC is closed so can't do much here...
	})
})

app.get('/kill/:pid', (req, res) => {
	shelljs.exec('./kill-vlc.sh ' + req.params.pid, function() {
		res.send('Killed. I think.')
	})
})

app.get('/play-pause', (req, res) => {
	shelljs.exec('./play-pause-vlc.sh')
	res.send('')
})

app.listen(port, () => {
  console.log(`VLC launcher listening at http://192.168.0.53:${port}`)
})
