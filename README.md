# VLC Launcher Server

This is quite rough around the edges (minimal POC hacked together quickly) with hard-coded values. It was made for a personal need and I'm not sure whether this will get any updates at all. Or it might develop into something.

Place this repository on the Raspberry Pi or whatever device you want to play from.

## Quick start

```
git clone git@gitlab.com:oscar.froberg/vlc-launcher-server.git
cd vlc-launcher-server
```

Edit `server.js` (change IP to your device's IP). Then proceed to install and run:

```
yarn install
node server.js
```

Now you should have a server running on `http://<ip>:4444`. If you visit that URL you can paste Youtube URLs and they should start playing on the device.

## Project structure

```
.
├── README.md
├── client.html
├── kill-vlc.sh
├── launch-vlc.sh
├── package.json
├── play-pause-vlc.sh
├── server.js
├── sftp-config.json
└── yarn.lock
```

### server.js

Web server running Node with [Express](https://expressjs.com/) for routing and [ShellJS](https://www.npmjs.com/package/shelljs) for running shell scripts.

### client.html

A web UI where you can add/play/pause videos as well as kill VLC processes.

### Shell scripts

The server uses shell scripts to interact with VLC.

```
launch-vlc.sh 		# launch URL in VLC
kill-vlc.sh 		# kill a running VLC process
play-pause-vlc.sh 	# play/pause VLC (uses dbus)
```
